import path from 'path'

export default {
  plugins: [
    [
      'react-static-plugin-source-filesystem',
      {
        location: path.resolve('./src/images'),
      },
    ],
  ],
}
