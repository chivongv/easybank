import React from "react";
import "./App.scss";
import { Navbar, Hero, Services, Articles, Footer } from "./components";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />
      <Services />
      <Articles />
      <Footer />
      <div className="attribution">
        Challenge by{" "}
        <a
          href="https://www.frontendmentor.io?ref=challenge"
          target="_blank"
          rel="noopener noreferrer"
        >
          Frontend Mentor
        </a>
        . Coded by <a href="www.chivongv.se">Chi Vong</a>.
      </div>
    </div>
  );
}

export default App;
