import React from "react";
import logo from "../images/logo-white.png";
import facebook from "../images/icon-facebook.svg";
import youtube from "../images/icon-youtube.svg";
import twitter from "../images/icon-twitter.svg";
import pinterest from "../images/icon-pinterest.svg";
import instagram from "../images/icon-instagram.svg";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__col1">
        <div className="footer__logo">
          <img src={logo} alt="Logo" />
        </div>
        <ul className="social-media">
          <li>
            <a href="/#" className="social__fb">
              <img src={facebook} alt="Facebook" />
            </a>
          </li>
          <li>
            <a href="/#" className="social__yt">
              <img src={youtube} alt="Youtube" />
            </a>
          </li>
          <li>
            <a href="/#" className="social__tw">
              <img src={twitter} alt="Twitter" />
            </a>
          </li>
          <li>
            <a href="/#" className="social__pi">
              <img src={pinterest} alt="Pinterest" />
            </a>
          </li>
          <li>
            <a href="/#" className="social__ig">
              <img src={instagram} alt="Instagram" />
            </a>
          </li>
        </ul>
      </div>
      
      <ul className="footer__nav-col1">
        <li>
          <a href="/">About Us</a>
        </li>
        <li>
          <a href="/">Contact</a>
        </li>
        <li>
          <a href="/">Blog</a>
        </li>
      </ul>
      <ul className="footer__nav-col2">
        <li>
          <a href="/">Careers</a>
        </li>
        <li>
          <a href="/">Support</a>
        </li>
        <li>
          <a href="/">Privacy Policy</a>
        </li>
      </ul>
      <div className="footer__col4">
        <a href="/" className="cta footer__cta">
          Request Invite
        </a>
        <div className="copyrights">© Easybank. All Rights Reserved</div>
      </div>
    </footer>
  );
};

export default Footer;
