import Navbar from './Navbar';
import Hero from './Hero';
import Services from './Services';
import Articles from './Articles';
import Footer from './Footer';

export { Navbar, Hero, Services, Articles, Footer };
