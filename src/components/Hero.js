import React from "react";
import bgIntroDesktop from "../images/bg-intro-desktop.svg";
import bgIntroMobile from "../images/bg-intro-mobile.svg";
import mockups from "../images/image-mockups.png";

const Hero = () => {
  return (
    <div className="hero">
      <header className="hero__heading">
        <h1 className="hero__title">Next generation <br /> digital banking</h1>
        <p className="hero__text">
          Take your financial life online. Your Easybank account will be a
          one-stop-shop for spending, saving, budgeting, investing, and much
          more.
        </p>
        <a href="/#" className="hero__cta">
          Request Invite
        </a>
      </header>
      <div className="hero__illustrations">
        <img src={bgIntroMobile} alt="Background" className="bg-mobile"/>
        <img src={bgIntroDesktop} alt="Background" className="bg-desktop" />
        <img src={mockups} alt="Phone mockups" className="phone-mockups" />
      </div>
    </div>
  );
};

export default Hero;
