import React from "react";
import ImgCurrency from "../images/image-currency.jpg";
import ImgRestaurant from "../images/image-restaurant.jpg";
import ImgPlane from "../images/image-plane.jpg";
import ImgConfetti from "../images/image-confetti.jpg";

const Articles = () => {
  return (
    <section className="articles">
      <h1 className="articles__title">Latest Articles</h1>
      <div className="articles__list">
        <div className="articles__item">
          <div className="articles__item-img">
            <img src={ImgCurrency} alt="Money" />
          </div>
          <div className="articles__item-content">
            <h4 className="articles__item-author">By Claire Robinson</h4>
            <h3 className="articles__item-title">
              Receive money in any currency with no fees
            </h3>
            <p className="articles__item-text">
              The world is getting smaller and we’re becoming more mobile. So
              why should you be forced to only receive money in a single …
            </p>
          </div>
        </div>
        <div className="articles__item">
          <div className="articles__item-img">
            <img src={ImgRestaurant} alt="Restaurant food" />
          </div>
          <div className="articles__item-content">
            <h4 className="articles__item-author">By Wilson Hutton</h4>
            <h3 className="articles__item-title">
              Treat yourself without worrying about money{" "}
            </h3>
            <p className="articles__item-text">
              Our simple budgeting feature allows you to separate out your
              spending and set realistic limits each month. That means you …
            </p>
          </div>
        </div>
        <div className="articles__item">
          <div className="articles__item-img">
            <img src={ImgPlane} alt="A plane" />
          </div>
          <div className="articles__item-content">
            <h4 className="articles__item-author">By Wilson Hutton</h4>
            <h3 className="articles__item-title">
              Take your Easybank card wherever you go
            </h3>
            <p className="articles__item-text">
              We want you to enjoy your travels. This is why we don’t charge any
              fees on purchases while you’re abroad. We’ll even show you …
            </p>
          </div>
        </div>
        <div className="articles__item">
          <div className="articles__item-img">
            <img src={ImgConfetti} alt="Confetti" />
          </div>
          <div className="articles__item-content">
            <h4 className="articles__item-author">By Claire Robinson</h4>
            <h3 className="articles__item-title">
              Our invite-only Beta accounts are now live!
            </h3>
            <p className="articles__item-text">
              After a lot of hard work by the whole team, we’re excited to
              launch our closed beta. It’s easy to request an invite through the
              site ...
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Articles;
