import React from "react";
import IconOnline from "../images/icon-online.svg";
import IconBudgeting from "../images/icon-budgeting.svg";
import IconOnboarding from "../images/icon-onboarding.svg";
import IconApi from "../images/icon-api.svg";

const Services = () => {
  return (
    <section className="services">
      <div className="services__heading">
        <h1 className="services__title">Why choose Easybank?</h1>
        <p className="services__title-text">
          We leverage Open Banking to turn your bank account into your financial
          hub. Control your finances like never before.
        </p>
      </div>
      <div className="services__list">
        <div className="services__item online">
          <div className="services_icon">
            <img src={IconOnline} alt="" />
          </div>
          <h3 className="services__subtitle">Online Banking</h3>
          <p className="services__text">
            Our modern web and mobile applications allow you to keep track of
            your finances wherever you are in the world.
          </p>
        </div>
        <div className="services__item budgeting">
          <div className="services_icon">
            <img src={IconBudgeting} alt="" />
          </div>
          <h3 className="services__subtitle">Simple Budgeting</h3>
          <p className="services__text">
            See exactly where your money goes each month. Receive notifications
            when you're close to hitting your limits.
          </p>
        </div>
        <div className="services__item onboarding">
          <div className="services_icon">
            <img src={IconOnboarding} alt="" />
          </div>
          <h3 className="services__subtitle">Fast Onboarding</h3>
          <p className="services__text">
            We don’t do branches. Open your account in minutes online and start
            taking control of your finances right away.
          </p>
        </div>
        <div className="services__item api">
          <div className="services_icon">
            <img src={IconApi} alt="" />
          </div>
          <h3 className="services__subtitle">Open API</h3>
          <p className="services__text">
            Manage your savings, investments, pension, and much more from one
            account. Tracking your money has never been easier.
          </p>
        </div>
      </div>
    </section>
  );
};

export default Services;
