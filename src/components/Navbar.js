import React from "react";
import logo from "../images/logo.svg";

const Header = () => {
  return (
    <nav className="navbar">
      <input type="checkbox" id="toggle-menu" />
      <div className="logo">
        <img src={logo} alt="Logo" />
      </div>

      <label htmlFor="toggle-menu" className="hamburger-menu">
        <span className="bar1"></span>
        <span className="bar2"></span>
        <span className="bar3"></span>
      </label>

      <ul className="navbar__navlinks">
        <li className="navbar__navitem">
          <a href="/#" className="navbar__navlink">
            Home
          </a>
        </li>
        <li className="navbar__navitem">
          <a href="/#" className="navbar__navlink">
            About
          </a>
        </li>
        <li className="navbar__navitem">
          <a href="/#" className="navbar__navlink">
            Contact
          </a>
        </li>
        <li className="navbar__navitem">
          <a href="/#" className="navbar__navlink">
            Blog
          </a>
        </li>
        <li className="navbar__navitem">
          <a href="/#" className="navbar__navlink">
            Careers
          </a>
        </li>
      </ul>

      <a href="/#" className="cta navbar__cta">
        Request Invite
      </a>

      <div className="overlay-mobile"></div>
    </nav>
  );
};

export default Header;
